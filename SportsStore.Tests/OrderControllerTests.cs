﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using SportsStore.Controllers;
using SportsStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SportsStore.Tests
{
    public class OrderControllerTests
    {
        [Fact]
        public void Cannot_Checkout_Empty_Cart()
        {
            Mock<IOrderRepository> mock = new Mock<IOrderRepository>();

            Cart cart = new Cart();

            Order order = new Order { };

            OrderController controller = new OrderController(mock.Object, cart);


            ViewResult result = controller.Checkout(new Order { }) as ViewResult;

            mock.Verify(m => m.SaveOrder(It.IsAny<Order>()), Times.Never);

            Assert.False(result.ViewData.ModelState.IsValid);

            Assert.True(string.IsNullOrEmpty(result.ViewName));
        }

        [Fact]
        public void Cannot_Checkout_Invalid_Shipping_Details()
        {
            Mock<IOrderRepository> mock = new Mock<IOrderRepository>();

            Cart cart = new Cart();

            cart.AddItem(new Product(), 1);

            Order order = new Order();

            OrderController target = new OrderController(mock.Object, cart);

            target.ModelState.AddModelError("error", "error");

            ViewResult result = target.Checkout(order) as ViewResult;

            mock.Verify(r => r.SaveOrder(It.IsAny<Order>()), Times.Never);

            Assert.True(string.IsNullOrEmpty(result.ViewName));

            Assert.False(result.ViewData.ModelState.IsValid);
        }

        [Fact]
        public void Can_Checkout_And_Submit()
        {
            Mock<IOrderRepository> mock = new Mock<IOrderRepository>();

            Order order = new Order { };

            Cart cart = new Cart();

            cart.AddItem(new Product(), 1);

            OrderController target = new OrderController(mock.Object, cart);

            RedirectToActionResult result = target.Checkout(order) as RedirectToActionResult;

            mock.Verify(r => r.SaveOrder(It.IsAny<Order>()), Times.Once);

            Assert.Equal("Completed", result.ActionName);

            
        }
    }
}
