﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using SportsStore.Controllers;
using SportsStore.Models;
using SportsStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SportsStore.Tests
{
    public class ProductControllerTests
    {
        [Fact]
        public void Can_Paginate()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();

            mock.Setup(m => m.Products).Returns((new Product[] {
                         new Product {ProductID = 1, Name = "P1"},
                         new Product {ProductID = 2, Name = "P2"},
                         new Product {ProductID = 3, Name = "P3"},
                         new Product {ProductID = 4, Name = "P4"},
                         new Product {ProductID = 5, Name = "P5"}
                         }).AsQueryable<Product>());

            ProductController controller = new ProductController(mock.Object);
            controller.PageSize = 3;

            ProductsListViewModel result =
                    controller.List(null, 2).ViewData.Model as ProductsListViewModel;
            // Assert
            Product[] prodArray = result.Products.ToArray();
            Assert.True(prodArray.Length == 2);
            Assert.Equal("P4", prodArray[0].Name);
            Assert.Equal("P5", prodArray[1].Name);

        }

        [Fact]
        public void Can_Send_Pagination_View_Model()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();

            mock.Setup(r => r.Products).Returns(new List<Product> {
                new Product {ProductID = 1, Name = "P1"},
                new Product {ProductID = 2, Name = "P2"},
                new Product {ProductID = 3, Name = "P3"},
                new Product {ProductID = 4, Name = "P4"},
                new Product {ProductID = 5, Name = "P5"}
            }.AsQueryable());

            ProductController controller = new ProductController(mock.Object)
            {
                PageSize = 3
            };

            ProductsListViewModel productsListViewModel = controller.List(null, 2).ViewData.Model as ProductsListViewModel;

            PagingInfo pagingInfo = productsListViewModel.PagingInfo;

            Assert.Equal(2, pagingInfo.CurrentPage);
            Assert.Equal(3, pagingInfo.ItemsPerPage);
            Assert.Equal(5, pagingInfo.TotalItems);
            Assert.Equal(2, pagingInfo.TotalPages);

        }

        [Fact]
        public void Can_Filter_Category()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns((new Product[] {
                new Product {ProductID = 1, Name = "P1", Category = "Cat1"},
                new Product {ProductID = 2, Name = "P2", Category = "Cat2"},
                new Product {ProductID = 3, Name = "P3", Category = "Cat1"},
                new Product {ProductID = 4, Name = "P4", Category = "Cat2"},
                new Product {ProductID = 5, Name = "P5", Category = "Cat3"}
            }).AsQueryable<Product>());

            var controller = new ProductController(mock.Object)
            {
                PageSize = 3
            };

            var result = controller.List("Cat2", 1).ViewData.Model as ProductsListViewModel;

            var products = result.Products.ToArray();

            Assert.Equal(2, products.Count());
            Assert.Equal("P2", products[0].Name);
            Assert.Equal("P4", products[1].Name);
            Assert.Equal("Cat2", products[1].Category);

        }

        [Fact]
        public void Generate_Category_Specific_Product_Count()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();

            mock.Setup(r => r.Products).Returns(new Product[] {
                new Product {ProductID = 1, Name = "P1", Category = "Cat1"},
                new Product {ProductID = 2, Name = "P2", Category = "Cat2"},
                new Product {ProductID = 3, Name = "P3", Category = "Cat1"},
                new Product {ProductID = 4, Name = "P4", Category = "Cat2"},
                new Product {ProductID = 5, Name = "P5", Category = "Cat3"}
            }.AsQueryable());

            ProductController controller = new ProductController(mock.Object);

            controller.PageSize = 3;

            Func<ViewResult, ProductsListViewModel> GetModel = (viewresult) => viewresult?.ViewData?.Model as ProductsListViewModel;

            int? res1 = GetModel(controller.List("Cat1")).PagingInfo.TotalItems;
            int? res2 = GetModel(controller.List("Cat2")).PagingInfo.TotalItems;
            int? res3 = GetModel(controller.List("Cat3")).PagingInfo.TotalItems;
            int? resall = GetModel(controller.List(null)).PagingInfo.TotalItems;

            Assert.Equal(2, res1);
            Assert.Equal(2, res2);
            Assert.Equal(1, res3);
            Assert.Equal(5, resall);

        }
    }
}
