﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.AspNetCore.Routing;
using Moq;
using SportsStore.Components;
using SportsStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SportsStore.Tests
{
    public class NavigationMenuViewComponentTests
    {
        [Fact]
        public void Can_Select_Categories()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();

            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product {ProductID = 1, Name = "P1", Category = "Apples"},
                new Product {ProductID = 2, Name = "P2", Category = "Apples"},
                new Product {ProductID = 3, Name = "P3", Category = "Plums"},
                new Product {ProductID = 4, Name = "P4", Category = "Oranges"}
            }.AsQueryable());


            var component = new NavigationMenuViewComponent(mock.Object);

            var categories = ((IEnumerable<string>)((component.Invoke() as ViewViewComponentResult).ViewData.Model)).ToArray();

            Assert.Equal("Apples", categories[0]);
            Assert.Equal("Oranges", categories[1]);
            Assert.Equal("Plums", categories[2]);
            Assert.True(categories.Count() == 3);
        }


        [Fact]
        public void Indicates_Selected_Category()
        {
            string selectedCategory = "Apples";

            Mock<IProductRepository> mock = new Mock<IProductRepository>();

            mock.Setup(m => m.Products).Returns((new Product[] {
                new Product {ProductID = 1, Name = "P1", Category = "Apples"},
                new Product {ProductID = 4, Name = "P2", Category = "Oranges"},
            }).AsQueryable<Product>());

            NavigationMenuViewComponent navigationMenuViewComponent = new NavigationMenuViewComponent(mock.Object);
            navigationMenuViewComponent.ViewComponentContext = new ViewComponentContext
            {
                ViewContext = new ViewContext
                {
                    RouteData = new RouteData()
                }
            };

            navigationMenuViewComponent.RouteData.Values["category"] = selectedCategory;

            string result = (navigationMenuViewComponent.Invoke() as ViewViewComponentResult).ViewData["SelectedCategory"].ToString();

            Assert.Equal(selectedCategory, result);

        }
    }
}
