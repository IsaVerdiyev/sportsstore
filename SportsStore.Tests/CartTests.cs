﻿using SportsStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SportsStore.Tests
{
    public class CartTests
    {

        [Fact]
        public void Can_Add_New_Lines()
        {
            Product product1 = new Product { ProductID = 1, Name = "P1" };
            Product product2 = new Product { ProductID = 2, Name = "P2" };

            Cart cart = new Cart();

            cart.AddItem(product1, 1);
            cart.AddItem(product2, 2);

            List<CartLine> lines = cart.Lines.ToList();

            Assert.Equal(2, lines.Count());
            Assert.True(product1 == lines[0].Product);
            Assert.Equal(product1, lines[0].Product);
            Assert.Equal(product2, lines[1].Product);
        }

        [Fact]
        public void Can_Add_Quantity_For_Existing_Line()
        {
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };

            Cart cart = new Cart();
            cart.AddItem(p1, 1);
            cart.AddItem(p2, 3);
            cart.AddItem(p1, 10);

            CartLine[] lines = cart.Lines.ToArray();

            Assert.Equal(2, lines.Length);
            Assert.Equal(11, lines[0].Quantity);
            Assert.Equal(3, lines[1].Quantity);
        }

        [Fact]
        public void Can_Remove_Line()
        {
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };
            Product p3 = new Product { ProductID = 3, Name = "P3" };


            Cart cart = new Cart();
            cart.AddItem(p1, 1);
            cart.AddItem(p2, 3);
            cart.AddItem(p3, 5);
            cart.AddItem(p2, 7);

            cart.RemoveLine(p2);

            CartLine[] lines = cart.Lines.OrderBy(p => p.Product.ProductID).ToArray();

            Assert.Equal(2, lines.Length);
            Assert.Equal("P1", lines[0].Product.Name);
            Assert.Equal("P3", lines[1].Product.Name);
            Assert.Empty(cart.Lines.Where(p => p.Product.ProductID == 2));
        }

        [Fact]
        public void Calculate_Cart_Total()
        {
            Product p1 = new Product { ProductID = 1, Name = "P1", Price = 100M };
            Product p2 = new Product { ProductID = 2, Name = "P2", Price = 200M };
            Product p3 = new Product { ProductID = 3, Name = "P3", Price = 300M };

            Cart cart = new Cart();

            cart.AddItem(p1, 2);
            cart.AddItem(p2, 1);
            cart.AddItem(p3, 2);
            cart.AddItem(p1, 1);

            decimal result = cart.ComputeTotalValue();

            Assert.Equal(1100M, result);
        }

        [Fact]
        public void Can_Clear_Cart()
        {
            Product p1 = new Product { ProductID = 1, Name = "P1", Price = 100M };
            Product p2 = new Product { ProductID = 2, Name = "P2", Price = 200M };
            Product p3 = new Product { ProductID = 3, Name = "P3", Price = 300M };

            Cart cart = new Cart();
            cart.AddItem(p1, 4);
            cart.AddItem(p2, 2);

            cart.Clear();

            Assert.Empty(cart.Lines);
        }
    }
}
