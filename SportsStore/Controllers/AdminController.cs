﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Controllers
{
    [Authorize]
    public class AdminController: Controller
    {
        private readonly IProductRepository repos;

        public AdminController(IProductRepository repos)
        {
            this.repos = repos;
        }

        public ViewResult Index() => View(repos.Products);

        public ViewResult Edit(int productId)
        {
            return View(repos.Products.FirstOrDefault(p => p.ProductID == productId));
        }

        [HttpPost]
        public IActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                repos.SaveProduct(product);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View(product);
            }
        }

        public ViewResult Create() => View(nameof(Edit), new Product());

        [HttpPost]
        public RedirectToActionResult  Delete(int productId)
        {
            Product product = repos.DeleteProduct(productId);

            if(product != null)
            {
                TempData["message"] = $"{product.Name} was deleted";
            }
            return RedirectToAction("Index");
        }
    }
}
