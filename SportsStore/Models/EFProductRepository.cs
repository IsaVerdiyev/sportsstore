﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class EFProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext context;

        public EFProductRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Product> Products => context.Products;

        public Product DeleteProduct(int productId)
        {
            Product product = context.Products.FirstOrDefault(p => p.ProductID == productId);

            if(product != null)
            {
                context.Remove(product);
                context.SaveChanges();
            }
            return product;

        }

        public void SaveProduct(Product product)
        {
            if(product.ProductID == 0)
            {
                context.Add(product);
            }
            else
            {
                Product pr = context.Products.FirstOrDefault(p => p.ProductID == product.ProductID);
                if(pr != null)
                {
                    pr.Name = product.Name;
                    pr.Category = product.Category;
                    pr.Description = product.Description;
                    pr.Price = product.Price;
                }
            }
            context.SaveChanges();
        }
    }
}
